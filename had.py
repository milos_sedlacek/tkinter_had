#!/usr/bin/python

from Tkinter import *
from random import randrange

root = Tk()
canvasWidth = 500
canvasHeight = 500

numberOfBoxes = 15
score = 0
started = False
gameState = None

class Box(object):
	boxObjects = {}
	lastBox = None
	speed = 500
	sizex = 20
	sizey = 20

	def __init__(self, x, y):
		#CLASS VARIABLES
		Box.boxObjects[id(self)] = self
		
		#INSTANCE VARIABLES
		self.sizex = Box.sizex
		self.sizey = Box.sizey
		self.colour = "red"
		self.parent = None
		self.child = None
		self.lastPosition = None
		self.inTow = []
		self.direction = (0, -20) #UP
				
		#METHODS
		self.create(x,y)
		self.overlap()

	
	def create(self, x, y):
		self.box = c.create_rectangle(x, y, x+self.sizex, y+self.sizey, fill=self.colour, tag=id(self))
		Box.lastBox = self
		self.updateLastPosition()
	
	def overlap(self):
		x1,y1,x2,y2 = c.coords(self.box)
		boxesUnder = c.find_enclosed(x1-1,y1-1,x2+1,y2+1)
		
		for boxUnder in boxesUnder:
			if int(c.gettags(boxUnder)[0]) in self.inTow:
				gameOver()
				
		for boxUnder in boxesUnder:
			if not boxUnder is self.box:
				if not int(c.gettags(boxUnder)[0]) in self.inTow:
					c.itemconfig(boxUnder, fill="blue")
					Box.boxObjects[int(c.gettags(boxUnder)[0])].makeChild(self)
	
	def outOfCanvas(self):
		x1,y1,x2,y2 = c.coords(self.box)
		if x1 > canvasWidth or x2 > canvasWidth or y1 > canvasHeight or y2 > canvasHeight:
			gameOver()
		if x1 < 0 or x2 < 0 or y1 < 0 or y2 < 0:
			gameOver()

	def addInTow(self, box):
		if self.parent is None:
			self.inTow.append(box)
		else:
			self.parent.addInTow(box)
	
	def makeChild(self, parent):
		if parent.child is None:
			if Box.speed > 50:
				Box.speed -= 50
			global score
			score += 1
			if score == numberOfBoxes:
				gameWin()
			self.parent = parent
			parent.child = self
			parent.addInTow(id(self))
			c.coords(self.box, *c.coords(parent.box))
		else:
			self.makeChild(parent.child)
		
	def updateLastPosition(self):
		self.lastPosition = c.coords(self.box)
	
	def move(self, direction=None):
		if direction is None:
			direction = self.direction
		
		global started
		if started:
			c.move(self.box, direction[0], direction[1])
			
			if self.child:
				self.child.moveToParentLastPosition()
			self.updateLastPosition()
			self.overlap()
			self.outOfCanvas()
			root.after(Box.speed, self.move)
	
	def moveToParentLastPosition(self):
		c.coords(self.box, *self.parent.lastPosition)
		if self.child:
			self.child.moveToParentLastPosition()
		self.updateLastPosition()

def createBox(positionX, positionY):
	Box(positionX, positionY)

def startGame(event):
	global gameState
	if not gameState:
		global started
		started = True
		Box.lastBox.move()
		instructions("HIDE")

def stopGame(event):
	global started
	started = False
	
def gameOver():
	global gameState
	gameState = "GAMEOVER"
	stopGame(None)
	c.create_text(canvasWidth/2, canvasHeight/2, fill="red", font=("Purisa",50), text="GAME OVER")
	c.create_text(canvasWidth/2, canvasHeight/2+40, fill="red", font=("Purisa",20), text="SCORE %d/%d" % (score, numberOfBoxes))
	
def gameWin():
	global gameState
	gameState = "WIN"
	stopGame(None)
	c.create_text(canvasWidth/2, canvasHeight/2, fill="green", font=("Purisa",50), text="YOU'VE WON!")
	c.create_text(canvasWidth/2, canvasHeight/2+40, fill="green", font=("Purisa",20), text="SCORE %d/%d" % (score, numberOfBoxes))

def instructions(showHide):
	if showHide == "SHOW":
		c.create_text(canvasWidth/2, canvasHeight/2, fill="blue", font=("Purisa",30), text="HOW TO PLAY", tag="instructions")
		c.create_text(canvasWidth/2, canvasHeight/2+40, fill="blue", font=("Purisa",15), text="YOU ARE THE GREEN BOX", tag="instructions")
		c.create_text(canvasWidth/2, canvasHeight/2+60, fill="blue", font=("Purisa",15), text="CLICK LMB TO START", tag="instructions")
		c.create_text(canvasWidth/2, canvasHeight/2+80, fill="blue", font=("Purisa",15), text="USE ARROW KEYS TO CHANGE DIRECTION", tag="instructions")
	else:
		c.delete("instructions")

def moveBox(event):
	direction = {
		"Up": (0,-20),
		"Down": (0,20),
		"Left": (-20,0),
		"Right": (20,0)}
	Box.lastBox.direction = direction.get(event.keysym)
	
def generatePostions():
	positionX = randrange(canvasWidth)+20
	positionY = randrange(canvasHeight)+20
	while positionX >= canvasWidth-Box.sizex:
		positionX -= 1
	while positionX % Box.sizex != 0:
		if positionX <= canvasWidth-Box.sizex:
			positionX += 1
		else:
			positionX -= 1
	
	while positionY >= canvasHeight-Box.sizey:
		positionY -= 1
	while positionY % Box.sizey != 0:
		if positionY <= canvasHeight-Box.sizey:
			positionY += 1
		else:
			positionY -= 1
	return (positionX, positionY)
	
def createGame():
	gameState = None
	score = 0
	usedPositions = []
	counter = numberOfBoxes+1
	while counter > 0:
		position = generatePostions()
		if not position in usedPositions:
			createBox(position[0], position[1])
			usedPositions.append(position)
			counter -= 1
	
	c.itemconfig(Box.lastBox.box, fill="green")
	instructions("SHOW")

c = Canvas(root, bg="white", width=canvasWidth, height=canvasHeight)

c.pack()
c.focus_set()
c.bind("<Button-3>", stopGame)
c.bind("<Button-1>", startGame)
c.bind("<Key>", moveBox)

createGame()
root.mainloop()